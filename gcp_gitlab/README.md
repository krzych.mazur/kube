Static web with letsencrypt ssl and kubernetes

## Environment

GitLab and GCP

## Plan

- Using GitLab to create kubernetes cluster with helm, certmanager, ingress
- Static website working on nginx container with configuration mounted as config map and website as NFS volume mount.
- Nginx Ingress for http traffic and SSL control
- Cert-Manager to support letsencrypt certificates

## Installation

For this purpose i used NGINX Ingress and Cert-Manager

NGINX Ingress
```bash
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: staticweb-ingress
  annotations:
    kubernetes.io/ingress.class: nginx
    certmanager.k8s.io/cluster-issuer: letsencrypt-staging
spec:
  tls:
  - hosts:
    - www.milky-way.cloud
    secretName: letsencrypt-staging
  rules:
  - host: www.milky-way.cloud
    http:
      paths:
      - backend:
          serviceName: mystaticweb
          servicePort: 80
```

Cert-Manager

```bash
apiVersion: certmanager.k8s.io/v1alpha1
kind: ClusterIssuer
metadata:
 name: letsencrypt-staging
spec:
 acme:
   server: https://acme-v02.api.letsencrypt.org/directory
   email: krzych.mazur@gmail.com
   privateKeySecretRef:
      name: letsencrypt-staging
   http01: {}
```

Static website service

```
apiVersion: v1
kind: Service
metadata:
  name: mystaticweb
spec:
  ports:
  - protocol: TCP
    port: 80
  selector:
    app: mystaticweb
```

Static website config map

```
apiVersion: v1
kind: ConfigMap
metadata:
    name: mystaticweb-cm
data:
  nginx.conf: |
    server {
        listen          443;
        server_name     www.milky-way.cloud
        location / {
                root   /usr/share/nginx/html;
                index  index.html index.htm;
        }
    }
    
```

Static website deploy
```
apiVersion: v1
kind: Service
metadata:
  name: mystaticweb
spec:
  ports:
  - protocol: TCP
    port: 80
  selector:
    app: mystaticweb
```


## Manifests

```bash
kubectl apply -f staticweb-svc.yaml
kubectl apply -f staticweb-cm.yaml
kubectl apply -f staticweb-deploy.yaml
kubectl apply -f letsencrypt_issuer.yaml
kubectl apply -f ingress-deploy.yaml
```
